package com.kazan;

import com.kazan.view.MyView;

public class App {

    public static void main(String[] args) {
        new MyView().start();
    }
}
