package com.kazan.model;

import com.kazan.Annotation.MyAnnotation;

public class Person {
    @MyAnnotation(name = "Artur")
    String name;

    @MyAnnotation(age = 18)
    int personAge;

    String city;

    public Person(String name, int personAge, String city) {
        this.name = name;
        this.personAge = personAge;
        this.city = city;
    }

    public Person() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPersonAge() {
        return personAge;
    }

    public void setPersonAge(int personAge) {
        this.personAge = personAge;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
